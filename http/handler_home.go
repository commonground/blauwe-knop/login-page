// Copyright © VNG Realisatie 2020
// Licensed under the EUPL

package http

import (
	"html/template"
	"log"
	"net/http"
	"path"
	"path/filepath"
)

type organization struct {
	Name        string
	CallbackURL string
}

func handlerHome(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()
	templateDirectory, _ := ctx.Value(htmlTemplateDirectoryKey).(string)

	templatePath := filepath.Join(templateDirectory, "home.html")
	filePath := path.Join(templatePath)

	organizationName := r.URL.Query().Get("organizationName")
	if organizationName == "" {
		log.Printf("organizationName missing")
		http.Error(w, "organizationName missing", http.StatusBadRequest)
		return
	}

	callbackUrlEncoded := r.URL.Query().Get("callbackURL")
	if callbackUrlEncoded == "" {
		log.Printf("callback URL missing")
		http.Error(w, "callback URL missing", http.StatusBadRequest)
		return
	}

	t, err := template.ParseFiles(filePath)
	if err != nil {
		log.Printf("error parsing template: %v", err)
		http.Error(w, "the page could not be loaded", http.StatusInternalServerError)
		return
	}

	err = t.Execute(w, &organization{
		Name:        organizationName,
		CallbackURL: callbackUrlEncoded,
	})

	if err != nil {
		log.Printf("error serving template: %v", err)
		http.Error(w, "the page could not be loaded", http.StatusInternalServerError)
		return
	}
}
