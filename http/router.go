// Copyright © VNG Realisatie 2020
// Licensed under the EUPL

package http

import (
	"context"
	"net/http"

	"github.com/go-chi/chi"
	healthcheck "gitlab.com/commonground/blauwe-knop/health-checker/healthchecker"
)

func NewRouter(htmlTemplateDirectory string) *chi.Mux {
	r := chi.NewRouter()

	r.Get("/", func(w http.ResponseWriter, r *http.Request) {
		ctx := context.WithValue(r.Context(), htmlTemplateDirectoryKey, htmlTemplateDirectory)
		handlerHome(w, r.WithContext(ctx))
	})
	r.Post("/login", func(w http.ResponseWriter, r *http.Request) {
		handlerLogin(w, r)
	})

	healthCheckHandler := healthcheck.NewHandler("login-page", []healthcheck.Checker{})
	r.Route("/health", func(r chi.Router) {
		r.Get("/", healthCheckHandler.HandleHealth)
		r.Get("/check", healthCheckHandler.HandlerHealthCheck)
	})

	return r
}

type key int

const (
	htmlTemplateDirectoryKey key = iota
)
