# Login page

Login page written in golang.

## Installation

Prerequisites:

- [Git](https://git-scm.com/)
- [Golang](https://golang.org/doc/install)
- [Modd](https://github.com/cortesi/modd)

1. Download the required Go dependencies:

```sh
go mod download
```

2. Now start the login page:

```sh
go run cmd/login-page/main.go --organization demo-org --redirect-url https://your-redirect.com
```

Or run the login page using modd, which wil restart the API on file changes.

```sh
modd
```

By default, the login page will run on port `8082`.

## Deployment

Prerequisites:

- [kubectl](https://kubernetes.io/docs/tasks/tools/install-kubectl/)
- [helm](https://helm.sh/docs/intro/)
- deployment access to the haven cluster `azure-common-prod`

You can use `helm` to deploy the login page

```
helm upgrade --install login-page ./charts/login-page -n bk-test
```
