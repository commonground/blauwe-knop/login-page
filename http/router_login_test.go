// Copyright © VNG Realisatie 2020
// Licensed under the EUPL

package http_test

import (
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"net/url"
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"

	http_infra "loginpage/http"
)

func Test_CreateRouter_Login(t *testing.T) {
	type args struct {
		bsn         string
		callbackURL string
	}
	tests := []struct {
		name                   string
		args                   args
		expectedHTTPStatusCode int
		expectedBody           string
		expectedRedirectUrl    string
	}{
		{
			"without bsn",
			args{
				bsn:         "",
				callbackURL: "",
			},
			http.StatusBadRequest,
			"bsn required\n",
			"",
		},
		{
			"without callbackURL",
			args{
				bsn:         "814859094",
				callbackURL: "",
			},
			http.StatusBadRequest,
			"callback URL required\n",
			"",
		},
		{
			"redirect after successfuly adding bsn to authtoken",
			args{
				bsn:         "814859094",
				callbackURL: "https://mock-callback",
			},
			http.StatusMovedPermanently,
			"",
			"https://mock-callback?bsn=814859094",
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {

			router := http_infra.NewRouter("")
			w := httptest.NewRecorder()

			form := url.Values{}
			form.Add("bsn", test.args.bsn)
			form.Add("callbackURL", test.args.callbackURL)

			request := httptest.NewRequest("POST", "/login", strings.NewReader(form.Encode()))
			request.PostForm = form
			request.Header.Add("Content-Type", "application/x-www-form-urlencoded")

			router.ServeHTTP(w, request)

			resp := w.Result()
			body, _ := ioutil.ReadAll(resp.Body)

			assert.Equal(t, test.expectedHTTPStatusCode, resp.StatusCode)
			if resp.StatusCode == http.StatusMovedPermanently {
				redirectUrl := resp.Header.Get("location")
				assert.Equal(t, test.expectedRedirectUrl, redirectUrl)
			} else {
				assert.Equal(t, test.expectedBody, string(body))
			}

		})
	}
}
