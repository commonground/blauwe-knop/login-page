// Copyright © VNG Realisatie 2020
// Licensed under the EUPL

package main

import (
	"log"
	"net/http"

	"github.com/svent/go-flags"

	http_infra "loginpage/http"
)

var options struct {
	HtmlTemplateDirectory string `long:"html-template-directory" env:"HTML_TEMPLATE_DIRECTORY" default:"html/templates/" description:"The directory in which the html templates can be found"`
	ListenAddress         string `long:"listen-address" env:"LISTEN_ADDRESS" default:"0.0.0.0:8082" description:"Address for the blauweknop api to listen on. Read https://golang.org/pkg/net/#Dial for possible tcp address specs."`
}

func main() {
	// Parse options
	args, err := flags.Parse(&options)
	if err != nil {
		if et, ok := err.(*flags.Error); ok {
			if et.Type == flags.ErrHelp {
				return
			}
		}
		log.Fatalf("error parsing flags: %v", err)
	}
	if len(args) > 0 {
		log.Fatalf("unexpected arguments: %v", args)
	}

	router := http_infra.NewRouter(options.HtmlTemplateDirectory)

	log.Printf("start listening on  %s", options.ListenAddress)
	err = http.ListenAndServe(options.ListenAddress, router)
	if err != nil {
		if err != http.ErrServerClosed {
			panic(err)
		}
	}
}
