module loginpage

go 1.19

require (
	github.com/go-chi/chi v1.5.4
	github.com/stretchr/testify v1.8.1
	github.com/svent/go-flags v0.0.0-20141123140740-4bcbad344f03
	gitlab.com/commonground/blauwe-knop/health-checker v0.0.2
)

require (
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
)
