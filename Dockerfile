# Use go 1.x based on alpine image.
FROM golang:1.19.4-alpine AS build

ADD . /go/src/loginpage/
ENV GO111MODULE on
WORKDIR /go/src/loginpage
RUN go mod download
RUN go build -o dist/bin/login-page ./cmd/login-page

# Release binary on latest alpine image.
FROM alpine:latest

COPY --from=build /go/src/loginpage/dist/bin/login-page /usr/local/bin/login-page
COPY --from=build /go/src/loginpage/html/templates/home.html /html/templates/home.html

ENV HTML_TEMPLATE_DIRECTORY "/html/templates"

# Add non-priveleged user
RUN adduser -D -u 1001 appuser
USER appuser
CMD ["/usr/local/bin/login-page"]
