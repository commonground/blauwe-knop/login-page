// Copyright © VNG Realisatie 2020
// Licensed under the EUPL

package http

import (
	"fmt"
	"log"
	"net/http"
	"net/url"
)

func handlerLogin(w http.ResponseWriter, req *http.Request) {
	err := req.ParseForm()
	if err != nil {
		log.Printf("error parsing form: %v", err)
		http.Error(w, "error parsing form", http.StatusBadRequest)
		return
	}

	bsn := req.Form.Get("bsn")
	if bsn == "" {
		log.Printf("bsn required")
		http.Error(w, "bsn required", http.StatusBadRequest)
		return
	}

	callBackUrlEncoded := req.Form.Get("callbackURL")
	if callBackUrlEncoded == "" {
		log.Printf("callback URL required")
		http.Error(w, "callback URL required", http.StatusBadRequest)
		return
	}

	callbackURLDecoded, err := url.QueryUnescape(callBackUrlEncoded)
	if err != nil {
		log.Printf("callback URL invalid")
		http.Error(w, "callback URL invalid", http.StatusBadRequest)
		return
	}

	http.Redirect(w, req, fmt.Sprintf("%s?bsn=%s", callbackURLDecoded, bsn), http.StatusMovedPermanently)
}
