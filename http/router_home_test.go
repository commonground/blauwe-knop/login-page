// Copyright © VNG Realisatie 2020
// Licensed under the EUPL

package http_test

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/stretchr/testify/assert"

	http_infra "loginpage/http"
)

func Test_CreateRouter_Home(t *testing.T) {
	type fields struct {
		htmlTemplateDirectory string
	}
	type args struct {
		organizationName string
		callbackURL      string
	}
	tests := []struct {
		name                   string
		fields                 fields
		args                   args
		expectedHTTPStatusCode int
		assertBody             bool
		expectedBody           string
	}{
		{
			"happy flow",
			fields{
				htmlTemplateDirectory: "../html/templates",
			},
			args{
				organizationName: "demo-org",
				callbackURL:      "callbackURL",
			},
			http.StatusOK,
			false,
			"",
		},
		{
			"mising organization name",
			fields{
				htmlTemplateDirectory: "../html/templates",
			},
			args{
				organizationName: "",
				callbackURL:      "callbackURL",
			},
			http.StatusBadRequest,
			true,
			"organizationName missing\n",
		},
		{
			"missing callback URL",
			fields{
				htmlTemplateDirectory: "../html/templates",
			},
			args{
				organizationName: "demo-org",
				callbackURL:      "",
			},
			http.StatusBadRequest,
			true,
			"callback URL missing\n",
		},
		{
			"invalid template",
			fields{
				htmlTemplateDirectory: "../testing/invalid-html-templates",
			},
			args{
				organizationName: "demo-org",
				callbackURL:      "callbackURL",
			},
			http.StatusInternalServerError,
			true,
			"the page could not be loaded\n",
		},
		{
			"non-existent template",
			fields{
				htmlTemplateDirectory: "../directory-which-does-not-exist",
			},
			args{
				organizationName: "demo-org",
				callbackURL:      "callbackURL",
			},
			http.StatusInternalServerError,
			true,
			"the page could not be loaded\n",
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			router := http_infra.NewRouter(test.fields.htmlTemplateDirectory)
			w := httptest.NewRecorder()

			request := httptest.NewRequest("GET", fmt.Sprintf("/?callbackURL=%s&organizationName=%s", test.args.callbackURL, test.args.organizationName), nil)

			router.ServeHTTP(w, request)

			resp := w.Result()
			body, _ := ioutil.ReadAll(resp.Body)
			assert.Equal(t, test.expectedHTTPStatusCode, resp.StatusCode)
			if test.assertBody {
				assert.Equal(t, test.expectedBody, string(body))
			}
		})
	}
}
